#Projet Python - Reddit/Arxiv
#Master 1 - 2020-2021

#Thomas Bibollet - Florian Gomez

##Introduction

Au sein du module de Programmation Avancée de notre première année de Master Informatique, nous devions réaliser un projet, en Python, pour extraire les données provenant de deux sites, créer des corpus de texte, et exécuter des analyses statistiques afin de pouvoir comparer des documents.
Les données proviennent de deux sites, Reddit et Arxiv. 
Reddit est un célèbre site communautaire, qu’on pourrait assimiler à un forum, classé par sous-catégories et dont le contenu est accessible à des millions d’utilisateurs. Reddit est un des sites Internet les plus utilisés au monde, surtout aux États-Unis. 
arXiv est une bibliothèque scientifique, accessible gratuitement, contenant plus d’1,5 millions de publications.  Elle traite de nombreux sujets, dans des domaines tels que les mathématiques ou l’informatique par exemple.

Le sujet que nous avons choisi, l’analyse comparative de corpus, nous a permis d’importer des corpus sur une interface globale, réalisée avec Tkinter. Nous avons donc créé une interface qui permet de créer un corpus en demandant à l’utilisateur un sujet désiré, puis renvoie plusieurs informations intéressantes sur le corpus, comme les mots les plus utilisés, ou les coefficients TF-IDF des termes importants.

(informations extraites du rapport fourni au format .docx)

Librairies requises : pickle, re, nltk, pandas, praw, irllib, xmltodict, sklearn, tkinter, matplotlib
